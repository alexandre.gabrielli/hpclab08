#pragma once

#include <stdint.h>
#include <xmmintrin.h>

struct xorshift32_state {
    uint32_t a;
};

uint32_t xorshift32(struct xorshift32_state *state);


void xordshift128(__m128i* four_states);
